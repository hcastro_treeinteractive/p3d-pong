import * as TWEEN from 'es6-tween'
import { UI } from 'p3d-three'

class UITitle extends UI.Canvas {
    constructor() {
        super(2048, 2048)

        //-------------------------------------
        // Title
        //-------------------------------------
        this.titleGroup = new UI.Element()
        this.addUIElement(this.titleGroup)

        const logo = new UI.Image('assets/logo-pong.png', 700, 340)
        logo.position.set(1024, 350)
        logo.anchor.set(0.5, 0.5)
        this.titleGroup.addChild(logo)
        // this.addUIElement(logo)

        this.titleText = this.createText(
            this.width / 2,
            670,
            'Press Right click or Space bar to Start',
            {
                fontSize: 53,
                style: 'bold',
                font: 'Roboto',
                alignment: 'left',
                color: 'black'
            }
        )
        this.titleText.anchor.set(0.5, 0.5)
        this.titleGroup.addChild(this.titleText)
        // this.addUIElement(this.titleText)

        //-------------------------------------
        // Reward
        //-------------------------------------
        this.rewardGroup = new UI.Element()
        this.rewardGroup.visible = false
        this.addUIElement(this.rewardGroup)

        this.congratsTitle = this.createText(1024, 560, 'CONGRATULATIONS!', {
            fontSize: 53,
            style: 'bold',
            font: 'Roboto',
            alignment: 'center',
            color: 'yellow'
        })
        //this.congratsTitle.anchor.set(0.5, 0.5)
        this.rewardGroup.addChild(this.congratsTitle)

        this.rewardText = this.createText(
            1024,
            650,
            'YOU WON THE MAGNIFICENT "OSOM" BALL PRIZE',
            {
                fontSize: 53,
                style: 'bold',
                font: 'Roboto',
                alignment: 'center',
                color: 'white'
            }
        )
        //this.rewardText.anchor.set(0.5, 0.5)
        this.rewardGroup.addChild(this.rewardText)

        this.rewardPlayText = this.createText(
            1024,
            1350,
            'PRESS SPACE BAR TO PLAY AGAIN',
            {
                fontSize: 53,
                style: 'bold',
                font: 'Roboto',
                alignment: 'center',
                color: 'white'
            }
        )
        //this.rewardText.anchor.set(0.5, 0.5)
        this.rewardGroup.addChild(this.rewardPlayText)

        //-------------------------------------
        //     ///IN GAME
        //-------------------------------------
        this.gameGroup = new UI.Element()
        this.gameGroup.visible = false
        this.addUIElement(this.gameGroup)

        this.gameTitle = this.createText(1024, 200, 'Game Score', {
            fontSize: 53,
            style: 'bold',
            font: 'Roboto',
            alignment: 'center',
            color: 'yellow'
        })
        //this.congratsTitle.anchor.set(0.5, 0.5)
        this.gameGroup.addChild(this.gameTitle)

        this.scoresText = this.createText(1024, 300, 'Player 1: 0 vs CPU: 0', {
            fontSize: 53,
            style: 'bold',
            font: 'Roboto',
            alignment: 'center',
            color: 'white'
        })
        //this.rewardText.anchor.set(0.5, 0.5)
        this.gameGroup.addChild(this.scoresText)
    }

    createPanel(x, y, w, h, color) {
        const r = new UI.Rect(w, h, color)
        r.position.set(x, y)
        return r
    }

    createImage(x, y, width, height, url) {
        const i = new UI.Image(url, width, height)
        i.position.set(x, y)
        return i
    }

    createText(x, y, text, style) {
        const label = new UI.Label(text, {
            style: style.style,
            fontSize: style.fontSize,
            font: style.font,
            textAlign: style.alignment,
            fillStyle: style.color,
            multiline: style.multiline || false,
            height: style.multiline ? style.height : style.fontSize,
            width: style.width,
            leading: style.leading
        })
        label.position.set(x, y)

        // Testing: Add a random color when clicking a label
        // label.inputEnabled = true
        // label.inputEvents.pressed = () => {
        //     let color = new THREE.Color(0xffffff)
        //     color.setHex(Math.random() * 0xffffff)
        //     label.fillStyle = '#' + color.getHexString()
        //     this.dirty = true
        // }
        return label
    }

    showCanvas(time) {
        time = time || 500
        const tween = new TWEEN.Tween(this.plane.material)
            .to({ opacity: 1 }, time)
            .easing(TWEEN.Easing.Quadratic.InOut)
        tween.start()
    }

    hideCanvas(time) {
        time = time || 500
        const tween = new TWEEN.Tween(this.material)
            .to({ opacity: 0 }, time)
            .easing(TWEEN.Easing.Quadratic.InOut)
        tween.start()
    }

    popTitle() {
        this.titleTween = new TWEEN.Tween(this.titleText.scale)
            .to({ x: 1.2, y: 1.2 }, 200)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .yoyo(true)
            .repeat(1)
            .on('update', () => {
                this.dirty = true
            })
            .start()
    }

    showReward(winner) {
        this.titleGroup.visible = false
        this.rewardGroup.visible = true
        this.gameGroup.visible = false

        if (winner === 'player') {
            this.congratsTitle.text = 'CONGRATULATIONS!'
            this.rewardText.text = 'YOU WON THE MAGNIFICENT "OSOM" BALL PRIZE'
        }
        else {
            this.congratsTitle.text = 'CPU WINS! YOU SUCK!!'
            this.rewardText.text = 'YOU MISSED THE INCREDIBLE "OSOM" BALL PRIZE'
        }
        this.dirty = true
    }

    showTitle() {
        this.titleGroup.visible = true
        this.rewardGroup.visible = false
        this.gameGroup.visible = false
    }

    showInGame() {
        this.titleGroup.visible = false
        this.rewardGroup.visible = false
        this.gameGroup.visible = true
    }

    updateInGameScore(player1, player2) {
        this.scoresText.text = 'Player 1: ' + player1 + ' vs CPU: ' + player2
        this.dirty = true
    }
}

export default UITitle
