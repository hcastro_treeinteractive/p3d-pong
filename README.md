# Requirements

*   node.js: https://nodejs.org/en/download/
*   yarn: https://yarnpkg.com/en/docs/install

# Recommended

*   Editor supporting eslint, e.g. VSCode https://code.visualstudio.com/
*   For VSCode install recommended extensions: Ctrl-Shift-P then `ext show recommend`

# Usage

*   Install dependencies: `yarn`
*   Start a developmentserver, watching changes: `yarn start`, then open browser at http://localhost:8080
*   Check for formatting / linting errors: `yarn lint`
