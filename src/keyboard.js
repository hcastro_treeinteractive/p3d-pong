window.addEventListener(
    'keyup',
    event => {
        Key.onKeyup(event)
    },
    false
)
window.addEventListener(
    'keydown',
    event => {
        Key.onKeydown(event)
    },
    false
)

export const Key = {
    _pressed: {},

    A: 65,
    W: 87,
    D: 68,
    S: 83,
    SPACE: 32,
    LEFT: 37,
    RIGHT: 39,
    UP: 38,
    DOWN: 83,

    isDown(keyCode) {
        return this._pressed[keyCode]
    },

    onKeydown(event) {
        this._pressed[event.keyCode] = true
    },

    onKeyup(event) {
        delete this._pressed[event.keyCode]
    }
}
