import * as THREE from 'three'
import * as TWEEN from 'es6-tween'
import { P3dMesh, P3dModelLoader } from 'p3d-three'
import P3dBaseApp from 'p3d-preact-plugin/components/p3d-base'
import pkg from '../package.json'
import UITitle from './uititle'
import Input from './input'
import REWARDS from './rewards.json'
import * as Util from './util'
import { Key } from './keyboard'

const MESH_ID_WHEEL = 'W38Q5'
const MESH_ID_BALL = 'Ke4QP'
const MESH_ID_PADDLE = 'Ct6pj'

/* const WHEEL_REWARD_DISTANCE = 0.765 */

const AppState = {
    Title: 0,
    InGame: 1,
    ShowingWinner: 2
}

export default class App extends P3dBaseApp {
    constructor() {
        super(pkg)
    }

    async setupScene() {
        this.state = AppState.Title

        // make transparency work a bit better
        this.renderer.sortObjects = false

        // field variables
        this.fieldWidth = 400
        this.fieldHeight = 200
        this.fieldDepth = 7
        this.bounceTime = 0

        // paddle variables
        this.paddleWidth = 0
        this.paddleHeight = 0
        this.paddleDepth = 0
        this.paddleQuality = 0
        this.paddle1DirY = 0
        this.paddle2DirY = 0
        this.paddleSpeed = 3

        // ball variables
        this.ballDirX = -1
        this.ballDirY = 1
        this.ballSpeed = 3

        // set the scene size
        const WIDTH = 1920
        const HEIGHT = 1080

        const VIEW_ANGLE = 70
        const ASPECT = WIDTH / HEIGHT
        const NEAR = 0.1
        const FAR = 10000

        // Create camera
        /*         this.camera.position.y = 1.5
        this.camera.position.z = 2.5 */
        this.camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR)
        //this.camera.lookAt(new THREE.Vector3())

        this.scene.add(this.camera)
        this.camera.position.z = 600

        this.modelLoader = new P3dModelLoader()

        // set up the playing surface plane
        this.planeWidth = this.fieldWidth
        this.planeHeight = this.fieldHeight
        this.planeQuality = 10

        // Create wheel of fortune
        this.tableGroup = new THREE.Group()
        this.scene.add(this.tableGroup)
        this.smallTable = new P3dMesh()
        this.smallTable.autoCenter = false
        this.modelLoader.load(this.smallTable, MESH_ID_WHEEL)

        this.smallTable.position.set(-2, 0, -105)
        this.smallTable.rotation.set(
            90 * THREE.Math.DEG2RAD,
            0 * THREE.Math.DEG2RAD,
            0 * THREE.Math.DEG2RAD
        )
        this.smallTable.scale.set(350, 500, 500)

        this.tableGroup.add(this.smallTable)

        this.planeMaterial = new THREE.MeshLambertMaterial({
            color: 0x4bd121
        })

        // create the playing surface plane
        this.plane = new THREE.Mesh(
            new THREE.PlaneGeometry(
                this.planeWidth * 0.95, // 95% of table width, since we want to show where the ball goes out-of-bounds
                this.planeHeight,
                this.planeQuality,
                this.planeQuality
            ),
            this.planeMaterial
        )

        this.scene.add(this.plane)
        this.plane.receiveShadow = true
        this.tableGroup.add(this.plane)

        this.ball = new P3dMesh()
        this.ball.autoCenter = false
        this.modelLoader.load(this.ball, MESH_ID_BALL)
        this.ball.scale.set(5, 5, 5)
        this.ball.position.set(0, 0, 0)
        this.ball.position.z = this.fieldDepth
        this.scene.add(this.ball)

        this.paddle1 = new P3dMesh()
        this.paddle1.autoCenter = false
        this.modelLoader.load(this.paddle1, MESH_ID_PADDLE)
        this.paddle1.scale.set(50, 50, 50)
        this.paddle1.position.set(0, 0.0, this.fieldDepth)
        this.paddle1.rotation.set(
            70 * THREE.Math.DEG2RAD,
            0 * THREE.Math.DEG2RAD,
            0
        )
        this.scene.add(this.paddle1)
        this.tableGroup.add(this.paddle1)

        const box = new THREE.Box3().setFromObject(this.paddle1)

        this.paddleWidth = box.max.x
        this.paddleHeight = box.max.y
        console.log(box.size())

        // set paddles on each side of the table
        this.paddle1.position.x = -this.fieldWidth / 2 + this.paddleWidth

        this.paddle2 = new P3dMesh()
        this.paddle2.autoCenter = false
        this.modelLoader.load(this.paddle2, MESH_ID_PADDLE)
        this.paddle2.scale.set(50, 50, 50)
        this.paddle2.position.set(0, 0.0, this.fieldDepth)
        this.paddle2.rotation.set(
            70 * THREE.Math.DEG2RAD,
            0 * THREE.Math.DEG2RAD,
            0
        )
        this.scene.add(this.paddle2)
        this.paddle2.position.x = this.fieldWidth / 2 - this.paddleWidth
        this.tableGroup.add(this.paddle2)

        // game-related variablesda
        this.score1 = 0
        this.score2 = 0
        // you can change this to any positive whole number
        this.maxScore = 7

        // set opponent reflexes (0 - easiest, 1 - hardest)
        this.difficulty = 0.2

        await this.modelLoader.wait()

        // Load the background texture
        const textureLoader = new THREE.TextureLoader()
        const texture = textureLoader.load('assets/BG2.jpg')
        const backgroundMesh = new THREE.Mesh(
            new THREE.PlaneGeometry(2, 2, 0),
            new THREE.MeshBasicMaterial({
                map: texture,
                depthTest: false,
                depthWrite: false
            })
        )

        // Create your background scene
        this.backgroundScene = new THREE.Scene()
        this.backgroundCamera = new THREE.Camera()
        this.backgroundScene.add(this.backgroundCamera)
        this.backgroundScene.add(backgroundMesh)

        // Add a ui canvas
        this.foregroundScene = new THREE.Scene()
        this.foregroundCamera = new THREE.Camera()

        this.uiTitle = new UITitle()
        this.uiTitle.scale.set(
            1.0 / this.canvas.width,
            1.0 / this.canvas.height,
            1
        )
        this.foregroundScene.add(this.uiTitle)

        // Initialize input
        this.input = new Input()
        this.input.camera = this.camera
        this.inputEnabled = true
        this.isDragging = false

        this.rotationSpeed = 0

        // create the pillar's material
        this.pillarMaterial = new THREE.MeshLambertMaterial({
            color: 0x534d0d
        })
        // create the ground's material
        this.groundMaterial = new THREE.MeshLambertMaterial({
            color: 0x888888
        })

        // we iterate 10x (5x each side) to create pillars to show off shadows
        // this is for the pillars on the left
        for (let i = 0; i < 5; i++) {
            const backdrop = new THREE.Mesh(
                new THREE.CubeGeometry(30, 30, 300, 1, 1, 1),

                this.pillarMaterial
            )

            backdrop.position.x = -50 + i * 100
            backdrop.position.y = 230
            backdrop.position.z = -30
            backdrop.castShadow = true
            backdrop.receiveShadow = true
            this.scene.add(backdrop)
        }
        // we iterate 10x (5x each side) to create pillars to show off shadows
        // this is for the pillars on the right
        for (let i = 0; i < 5; i++) {
            const backdrop = new THREE.Mesh(
                new THREE.CubeGeometry(30, 30, 300, 1, 1, 1),

                this.pillarMaterial
            )

            backdrop.position.x = -50 + i * 100
            backdrop.position.y = -230
            backdrop.position.z = -30
            backdrop.castShadow = true
            backdrop.receiveShadow = true
            this.scene.add(backdrop)
        }

        // finally we finish by adding a ground plane
        // to show off pretty shadows
        this.ground = new THREE.Mesh(
            new THREE.CubeGeometry(1000, 1000, 3, 1, 1, 1),

            this.groundMaterial
        )
        // set ground to arbitrary z position to best show off shadowing
        this.ground.position.z = -132
        this.ground.receiveShadow = true
        this.scene.add(this.ground)
        this.tableGroup.add(this.ground)

        // // create a point light
        this.pointLight = new THREE.PointLight(0xf8d898)

        // set its position
        this.pointLight.position.x = -1000
        this.pointLight.position.y = 0
        this.pointLight.position.z = 1000
        this.pointLight.intensity = 2.9
        this.pointLight.distance = 10000
        // add to the scene
        this.scene.add(this.pointLight)

        // add a spot light
        // this is important for casting shadows
        this.spotLight = new THREE.SpotLight(0xf8d898)
        this.spotLight.position.set(0, 0, 460)
        this.spotLight.intensity = 1.5
        this.spotLight.castShadow = true
        this.scene.add(this.spotLight)

        // MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC
        // renderer.shadowMapEnabled = true
        //this.scene.add(new THREE.AxisHelper(20))
    }

    resize() {
        super.resize()

        if (this.uiTitle) {
            this.uiTitle.scale.set(
                1.0 / this.canvas.width,
                1.0 / this.canvas.height,
                1
            )
        }
    }

    // Handles camera and lighting logic
    cameraPhysics() {
        // we can easily notice shadows if we dynamically move lights during the game
        this.spotLight.position.x = this.ball.position.x * 2
        this.spotLight.position.y = this.ball.position.y * 2

        // move to behind the player's paddle
        this.camera.position.x = this.paddle1.position.x - 100
        this.camera.position.y +=
            (this.paddle1.position.y - this.camera.position.y) * 0.05
        this.camera.position.z =
            this.paddle1.position.z +
            100 +
            0.04 * (-this.ball.position.x + this.paddle1.position.x)

        // rotate to face towards the opponent
        this.camera.rotation.x = -0.01 * this.ball.position.y * Math.PI / 180
        this.camera.rotation.y = -60 * Math.PI / 180
        this.camera.rotation.z = -90 * Math.PI / 180
    }

    beforeRender() {
        this.renderer.autoClear = false
        this.renderer.clear()
        this.renderer.render(this.backgroundScene, this.backgroundCamera)
    }

    afterRender() {
        this.renderer.render(this.foregroundScene, this.foregroundCamera)
    }

    update(dt) {
        this.uiTitle.update()

        switch (this.state) {
        case AppState.Title:
            if (this.input.mouse.justPressed || Key.isDown(Key.SPACE)) {
                this.state = AppState.InGame
                //new TWEEN.Tween(this.uiTitle.material).to({ opacity: 1 }).start()
                this.uiTitle.popTitle()

                /*       new TWEEN.Tween(this.uiTitle.material)
                    .to({ opacity: 0 })
                    .easing(TWEEN.Easing.Quadratic.InOut)
                    .start() */

                /*          new TWEEN.Tween(this.uiTitle.material)
                    .to({ opacity: 1 })
                    .easing(TWEEN.Easing.Quadratic.InOut)
                    .start()
               */
                this.uiTitle.showInGame()
            }

            break
        case AppState.InGame:
            //this.uiTitle.showInGame()
            // Decay rotation speed
            this.ballPhysics(dt)
            this.paddlePhysics()
            this.cameraPhysics()
            this.playerPaddleMovement()
            this.opponentPaddleMovement()
            break
        case AppState.ShowingWinner:
            Util.rotateOnWorldAxis(
                this.ball,
                new THREE.Vector3(0, 1, 0),
                this.input.getDragDelta().x
            )
            Util.rotateOnWorldAxis(
                this.ball,
                new THREE.Vector3(-1, 0, 0),
                this.input.getDragDelta().y
            )
            if (this.input.mouse.jusPressed || Key.isDown(Key.SPACE)) {
                this.restart()
            }
            break
        }

        this.input.update()
        TWEEN.update()
    }

    showWinner(winner) {
        this.uiTitle.showReward(winner)
        this.tableGroup.visible = false
        //this..visible = false
        //this.handle.visible = false
        this.uiTitle.material.opacity = 1

        /*         this.ball2 = new P3dMesh()
        this.ball2.autoCenter = false
        this.modelLoader.load(this.ball2, MESH_ID_BALL)
        this.ball2.scale.set(100, 100, 100)
        this.ball2.position.set(0, 0, 0)
        this.ball2.position.z = 100
        this.scene.add(this.ball2) */
        this.ball.scale.set(150, 150, 150)
        this.ball.rotation.set(0, 160, 0)
        new TWEEN.Tween(this.camera.position)
            .to({ y: 1.5, z: 2.5 })
            .easing(TWEEN.Easing.Quadratic.InOut)
            .start()

        //const targetPosition = new THREE.Vector3()
        new TWEEN.Tween(this.ball.position)
            .to({ z: 0 })
            .easing(TWEEN.Easing.Quadratic.InOut)
            .on('update', () => {
                this.camera.lookAt(this.ball.position)
            })
            .start()

        /*  this.ball.position.x = 0
            this.ball.position.y = 0 */

        this.state = AppState.ShowingWinner
    }

    restart() {
        this.uiTitle.updateInGameScore('0', '0')
        this.uiTitle.showTitle()
        // ball variables

        this.score1 = 0
        this.score2 = 0
        this.resetBall(1)
        // ball variables
        this.ballDirX = -1
        this.ballDirY = 1
        this.ballSpeed = 3
        this.ball.scale.set(11, 11, 11)
        this.ball.position.z = this.fieldDepth

        this.tableGroup.visible = true
        this.ball.visible = true
        new TWEEN.Tween(this.uiTitle.material).to({ opacity: 1 }).start()
        this.state = AppState.Title
    }

    ballPhysics(dt) {
        this.ball.rotation.y += THREE.Math.DEG2RAD * 200 * dt
        this.ball.rotation.z += THREE.Math.DEG2RAD * 200 * dt
        this.ball.rotation.x += THREE.Math.DEG2RAD * 200 * dt

        // if ball goes off the 'left' side (Player's side)
        if (this.ball.position.x <= -this.fieldWidth / 2) {
            // CPU scores
            this.score2++
            // update scoreboard HTML
            //document.getElementById('scores').innerHTML = score1 + '-' + score2
            // reset ball to center
            this.uiTitle.updateInGameScore(this.score1, this.score2)
            this.resetBall(2)
            this.matchScoreCheck()
        }

        // if ball goes off the 'right' side (CPU's side)
        if (this.ball.position.x >= this.fieldWidth / 2) {
            // Player scores
            this.score1++
            // update scoreboard HTML
            //document.getElementById('scores').innerHTML = score1 + '-' + score2
            // reset ball to center
            this.uiTitle.updateInGameScore(this.score1, this.score2)
            this.resetBall(1)
            this.matchScoreCheck()
        }

        // if ball goes off the top side (side of table)
        if (this.ball.position.y <= -this.fieldHeight / 2) {
            this.ballDirY = -this.ballDirY
        }
        // if ball goes off the bottom side (side of table)
        if (this.ball.position.y >= this.fieldHeight / 2) {
            this.ballDirY = -this.ballDirY
        }

        // update ball position over time
        this.ball.position.x += this.ballDirX * this.ballSpeed
        this.ball.position.y += this.ballDirY * this.ballSpeed

        // limit ball's y-speed to 2x the x-speed
        // this is so the ball doesn't speed from left to right super fast
        // keeps game playable for humans
        if (this.ballDirY > this.ballSpeed * 2) {
            this.ballDirY = this.ballSpeed * 2
        }
        else if (this.ballDirY < -this.ballSpeed * 2) {
            this.ballDirY = -this.ballSpeed * 2
        }
    }

    // Handles CPU paddle movement and logic
    opponentPaddleMovement() {
        // Lerp towards the ball on the y plane
        this.paddle2DirY =
            (this.ball.position.y - this.paddle2.position.y) * this.difficulty

        // in case the Lerp function produces a value above max paddle speed, we clamp it
        if (Math.abs(this.paddle2DirY) <= this.paddleSpeed) {
            this.paddle2.position.y += this.paddle2DirY
        }
        // if the lerp value is too high, we have to limit speed to paddleSpeed
        else {
            // if paddle is lerping in +ve direction
            if (this.paddle2DirY > this.paddleSpeed) {
                this.paddle2.position.y += this.paddleSpeed
            }
            // if paddle is lerping in -ve direction
            else if (this.paddle2DirY < -this.paddleSpeed) {
                this.paddle2.position.y -= this.paddleSpeed
            }
        }
        // We lerp the scale back to 1
        // this is done because we stretch the paddle at some points
        // stretching is done when paddle touches side of table and when paddle hits ball
        // by doing this here, we ensure paddle always comes back to default size
        // this.paddle2.scale.y += (1 - this.paddle2.scale.y) * 0.2
    }

    resetBall(loser) {
        // position the ball in the center of the table
        this.ball.position.x = 0
        this.ball.position.y = 0
        this.ball.position.z = this.fieldDepth

        // if player lost the last point, we send the ball to opponent
        if (this.loser === 1) {
            this.ballDirX = -1
        }
        // else if opponent lost, we send ball to player
        else {
            this.ballDirX = 1
        }

        // set the ball to move +ve in y plane (towards left from the camera)
        this.ballDirY = 1
    }

    // Handles player's paddle movement
    playerPaddleMovement() {
        //ad move left
        if (Key.isDown(Key.A) || Key.isDown(Key.LEFT)) {
            // if paddle is not touching the side of table
            // we move
            if (this.paddle1.position.y < this.fieldHeight * 0.45) {
                this.paddle1DirY = this.paddleSpeed
            }
            // else we don't move and stretch the paddle
            // to indicate we can't move
            else {
                this.paddle1DirY = 0
                // this.paddle1.scale.z += (10 - this.paddle1.scale.z) * 0.2
            }
        }
        // move right
        else if (Key.isDown(Key.D) || Key.isDown(Key.RIGHT)) {
            // if paddle is not touching the side of table
            // we move
            if (this.paddle1.position.y > -this.fieldHeight * 0.45) {
                this.paddle1DirY = -this.paddleSpeed
            }
            // else we don't move and stretch the paddle
            // to indicate we can't move
            else {
                this.paddle1DirY = 0
                // this.paddle1.scale.z += (10 - this.paddle1.scale.z) * 0.2
            }
        }
        // else don't move paddle
        else {
            // stop the paddle
            this.paddle1DirY = 0
        }

        /*     this.paddle1.scale.y += (1 - this.paddle1.scale.y) * 0.2
        this.paddle1.scale.z += (1 - this.paddle1.scale.z) * 0.2 + 75 */
        this.paddle1.position.y += this.paddle1DirY
    }

    // Handles paddle collision logic
    paddlePhysics() {
        // PLAYER PADDLE LOGIC

        // if ball is aligned with paddle1 on x plane
        // remember the position is the CENTER of the object
        // we only check between the front and the middle of the paddle (one-way collision)
        if (
            this.ball.position.x <=
                this.paddle1.position.x + this.paddleWidth &&
            this.ball.position.x >= this.paddle1.position.x
        ) {
            // and if ball is aligned with paddle1 on y plane
            if (
                this.ball.position.y <=
                    this.paddle1.position.y + this.paddleHeight / 2 &&
                this.ball.position.y >=
                    this.paddle1.position.y - this.paddleHeight / 2
            ) {
                // and if ball is travelling towards player (-ve direction)
                if (this.ballDirX < 0) {
                    // stretch the paddle to indicate a hit
                    // this.paddle1.scale.y = 15
                    // switch direction of ball travel to create bounce
                    this.ballDirX = -this.ballDirX
                    // we impact ball angle when hitting it
                    // this is not realistic physics, just spices up the gameplay
                    // allows you to 'slice' the ball to beat the opponent
                    this.ballDirY -= this.paddle1DirY * 0.7
                }
            }
        }

        // OPPONENT PADDLE LOGIC

        // if ball is aligned with paddle2 on x plane
        // remember the position is the CENTER of the object
        // we only check between the front and the middle of the paddle (one-way collision)
        if (
            this.ball.position.x <=
                this.paddle2.position.x + this.paddleWidth &&
            this.ball.position.x >= this.paddle2.position.x
        ) {
            // and if ball is aligned with paddle2 on y plane
            if (
                this.ball.position.y <=
                    this.paddle2.position.y + this.paddleHeight / 2 &&
                this.ball.position.y >=
                    this.paddle2.position.y - this.paddleHeight / 2
            ) {
                // and if ball is travelling towards opponent (+ve direction)
                if (this.ballDirX > 0) {
                    // stretch the paddle to indicate a hit
                    // this.paddle2.scale.y = 15
                    // switch direction of ball travel to create bounce
                    this.ballDirX = -this.ballDirX
                    // we impact ball angle when hitting it
                    // this is not realistic physics, just spices up the gameplay
                    // allows you to 'slice' the ball to beat the opponent
                    this.ballDirY -= this.paddle2DirY * 0.7
                }
            }
        }
    }

    // checks if either player or opponent has reached 7 points
    matchScoreCheck() {
        // if player has 7 points
        if (this.score1 >= this.maxScore) {
            // stop the ball
            this.ballSpeed = 0
            // write to the banner
            // make paddle bounce up and down
            this.bounceTime++
            this.paddle1.position.z = Math.sin(this.bounceTime * 0.1) * 10
            this.showWinner('player')
            // enlarge and squish paddle to emulate joy
            /* 		this.paddle1.scale.z = 2 + Math.abs(Math.sin(this.bounceTime * 0.1)) * 10;
		this.paddle1.scale.y = 2 + Math.abs(Math.sin(this.bounceTime * 0.05)) * 10; */
        }
        // else if opponent has 7 points
        else if (this.score2 >= this.maxScore) {
            // stop the ball
            this.ballSpeed = 0
            // write to the banner
            // make paddle bounce up and down
            this.bounceTime++
            this.paddle2.position.z = Math.sin(this.bounceTime * 0.1) * 10
            this.showWinner('cpu')
            // enlarge and squish paddle to emulate joy
            /* 		this.paddle2.scale.z = 2 + Math.abs(Math.sin(this.bounceTime * 0.1)) * 10;
		this.paddle2.scale.y = 2 + Math.abs(Math.sin(this.bounceTime * 0.05)) * 10; */
        }
    }
}
